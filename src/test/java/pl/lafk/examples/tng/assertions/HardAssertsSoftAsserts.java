package pl.lafk.examples.tng.assertions;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

/**
 * When Stanisław Lem was 81 years old, entire front-page was dedicated to him in GK (Gazeta Krakowska).
 * Printing imp chose an excellent moment to prank: Pisarz -> Pi*darz.
 * If only they had this test... WITH soft assertions of course.
 *
 * @author Tomasz @LAFK_pl Borek
 * @see _SimpleAsserts for previous
 * @see SimpleAssertsInspected next
 */
@Test
public class HardAssertsSoftAsserts {

    private Person pusta = new Person();

    public void theHardWay() {
        assertNotNull(pusta.getName(), "null name");
        assertNotNull(pusta.getSurname(), "null surname");
        assertNotNull(pusta.getJob(), "null job");
        assertNotEquals(pusta.getAge(), 0, "default age for an int field: 0");
        assertNotNull(pusta.toString(), "toString null");
    }

    public void theSoftWay() {
        SoftAssert sa = new SoftAssert();
        sa.assertNotNull(pusta.getName(), "null name");
        sa.assertNotNull(pusta.getSurname(), "null surname");
        sa.assertNotNull(pusta.getJob(), "null job");
        sa.assertNotEquals(pusta.getAge(), 0, "default age for an int field: 0");
        sa.assertNotNull(pusta.toString(), "toString null");
        sa.assertAll();
    }

    public void isStanisławLemFilledAllRight() {
        Person pisarzStanisławLem81 = new Person("pizdarz", "Stanisław",
                "Lem", 81);
        //TODO: Prevent Malicious Print-Imp from pranking Stanisław Lem and our Gazette!

    }
}
