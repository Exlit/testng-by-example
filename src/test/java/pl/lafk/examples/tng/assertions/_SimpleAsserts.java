package pl.lafk.examples.tng.assertions;

import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static org.testng.Assert.assertFalse;

/**
 * TestNG basic assertions are grouped in pairs.
 *
 * @implNote the order is actual - expected - message, not like in JUnit.
 * @author Tomasz @LAFK_pl Borek
 * @see HardAssertsSoftAsserts next
 */
@Test
public class _SimpleAsserts {

    public void trueOrFalseNoMessage() {
        assertTrue(true);
        assertFalse(false);
    }

    //TODO: fail this test on assertion to demo the message
    public void trueOrFalse() {
        assertTrue(true, "Breaking news! true wasn't true");
        assertFalse(false, "Breaking news! false was not false");
    }

    public void equalsOrNot() {
        assertEquals(1, 1);
        assertNotEquals(1==1, false);
    }

    //TODO: fail this test on assertion to demo the message
    public void equalsOrNotWithMessage() {
        assertEquals(1, 1, "1 was not 1");
        assertNotEquals(1==1, false, "this message should contain extra information that is useful when looking at a failure");
    }

    public void sameOrNot() {
        assertSame(1, 1, "1 and 1 actually not same");
        assertNotSame(new Integer(1), new Integer(1), "1 and 1 actually same");
    }

    public void nullOrNot() {
        assertNull(null, "Reporting! Null actually is not null!");
        assertNotNull(new Integer(1), "Reporting! Null actually is not null!");
    }

    public void tellAboutOtherAssertions_Deep_NoOrder() {
    }
}
