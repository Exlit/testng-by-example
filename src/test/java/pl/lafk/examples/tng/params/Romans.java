package pl.lafk.examples.tng.params;

/**
 * @author Tomasz @LAFK_pl Borek
 */
class Romans {

    static String from(int arabicNr) {
        switch (arabicNr) {
            case 1:
                return "I".repeat(1);
            case 2:
                return "I".repeat(2);
            case 3:
                return "I".repeat(3);
            case 4:
                return "IV";
            case 5:
                return "V";

        }
        return "O";
    }
}
