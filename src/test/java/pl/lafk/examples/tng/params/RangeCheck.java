package pl.lafk.examples.tng.params;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.stream.IntStream;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Are numbers in range? is a common problem in input validation
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class RangeCheck {

    @DataProvider
    public static Object[] inRange() {
        return IntStream.rangeClosed(-3,3).boxed().toArray(Object[]::new);
    }

    @Test(dataProvider = "inRange")
    public void fineForIntsInRange(int i) {
        assertTrue(i <= 3 && i >= -3, "number not in <-3,3> range");
    }

    @DataProvider
    public static Object[] notInRange() {
        return IntStream.rangeClosed(-200_000_000,-3).boxed().toArray(Object[]::new);
    }

    @Test(dataProvider = "notInRange")
    public void forIntsNotInRange(int i) {
        assertTrue(i > 3 || i < -3, "number in <-3,3> range");
    }

    @DataProvider
    public static Iterator<Integer> notInRangeViaIterator() {
        return IntStream.rangeClosed(Integer.MIN_VALUE,-3).iterator();
    }

}
