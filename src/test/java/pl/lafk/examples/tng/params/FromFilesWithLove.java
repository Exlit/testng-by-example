package pl.lafk.examples.tng.params;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import static org.testng.Assert.assertTrue;

/**
 * Pulls names from a file
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class FromFilesWithLove {

    @DataProvider
    public Iterator<String> iterateThroughNamesList() {
        return List.of("Ala", "Ola", "Wiola").iterator();
    }

    @Test(dataProvider = "iterateThroughNamesList")
    public void doThoseNamesContainA(String name) {
        assertTrue(name.contains("a"), "name without the letter a");
    }

    @DataProvider
    public Iterator<String> loadNamesFromFile() throws URISyntaxException, IOException {
        return Files.readAllLines(Paths
                .get(ClassLoader.getSystemResource("names.data").toURI())).iterator();
    }


    @Test(dataProvider = "loadNamesFromFile")
    public void famousNames(String name) {
        System.out.println(name);
    }
}
