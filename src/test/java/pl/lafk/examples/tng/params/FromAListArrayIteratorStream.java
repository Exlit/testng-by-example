package pl.lafk.examples.tng.params;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Lazy or not? Here I come!
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class FromAListArrayIteratorStream {

    @DataProvider
    public String[] fromAnArray() {
        return new String[]{"Ala", "Ola", "Wiola"};
    }

    @DataProvider
    public Object[] fromAnArrayOfObjects() {
        return new Object[]{"Ala", "Ola", "Ela"};
    }

    @Test(dataProvider = "fromAnArray")
    public void namesStartWithACapitalLetter(String name) {
        assertTrue(Character.isUpperCase(name.charAt(0)), "name isn't capitalized");
    }

    @DataProvider
    public Iterator<String> iterateThroughNamesList() {
        return List.of("Ala", "Ola", "Wiola").iterator();
    }

    @Test(dataProvider = "iterateThroughNamesList")
    public void doThoseNamesContainA(String name) {
        assertTrue(name.contains("a"), "name without the letter a");
    }

    @DataProvider
    public Iterator<String> threeLettersLongNames() {
        return Stream.of("Ala", "Ola", "Ela", "Wiola").iterator();
    }

    @Test(dataProvider = "threeLettersLongNames")
    public void allNames3Letters(String name) {
        assertEquals(name.length(), 3, "name length not 3");
    }

    //FIXME: can't parametrize Stream (Stream<String>) - throws ClassCastException
    @DataProvider
    public Iterator<Stream<String>> threeLettersLongNamesViaStream() {
        return Arrays.asList(Stream.of("Ala", "Ola", "Ela", "Wiola")).iterator();
    }

}
