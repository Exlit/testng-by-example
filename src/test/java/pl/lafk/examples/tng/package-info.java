/**
 * Features demonstrated:
 * <h2>basics: {@link pl.lafk.examples.tng.assertions assertions} package</h2>
 * <li>simple assertions,</li>
 * <li>soft assertions,</li>
 * <li>IDE inspections for TestNG assertions.</li>
 * <h2>basics: test results in {@link pl.lafk.examples.tng.results results} package</h2>
 * <li>skipping tests in the code due to some conditions,</li>
 * <li>launching tests in an annotated class or via annotated methods,</li>
 * <li>test results: ignored, skipped, disabled,</li>
 * <li>test results: success,</li>
 * <li>test results: failed and caught by assertion,</li>
 * <li>test results: failed in tested code,</li>
 * <li>test results and tricky throwing,</li>
 * <li>testing the throwing itself,</li>
 * <li>testing traps,</li>
 * <li>testNG test templates,</li>
 * <li>testing exceptions.</li>
 * <h2>parametrized tests: {@link pl.lafk.examples.tng.params params} package</h2>
 * <li>parametrized tests: DataProvider in FizzBuzz,</li>
 * <li>parametrized tests: DataProvider in Roman Numerals,</li>
 * <li>parametrized tests: DataProvider and inspections,</li>
 * <li>parametrized tests: DataProvider where can I take the data from,</li>
 * <li>parametrized tests: DataProvider mem usage (RangeCheck, ImpactCheck),</li>
 * <li>FIXME: parametrized tests: DataProvider Indices,</li>
 * TODO: generating tests via tests,
 * TODO: Parameters
 * TODO: Factory
 * TODO: Factory in a DataProvider
 *
 * <h2>Dependent tests: {@link pl.lafk.examples.tng.deps deps} package</h2>
 * ordering test methods
 * <li>test methods dependent on other tests methods</li>
 * <li>unless despite this dependency they need to execute</li>
 * <li>group of tests</li>
 * <li>group of tests dependent on other group of tests</li>
 * <li>except if we don't want some tests to be dependent because we so fancy</li>
 *
 * <h2>Not done in code yet</h2>
 * moving a method from a group higher in order of execution of all tests methods
 * controlling test execution programmatically and via XML and via annotations
 * controlling test results programmatically and via XML and via annotations
 * if time permits it - plugging in and modifying test execution on the fly
 * intercepting test results
 * better reports
 * auto-customization of test reports
 * multithreaded tests.
 */
package pl.lafk.examples.tng;
